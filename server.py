from flask import Flask
from flask_restful import Api

import router
import os

app = Flask(__name__)
api = Api(app)

router.createRoutes(api)

if 'APP_ENV' in os.environ and os.environ['APP_ENV'] == 'prod':
    print('Starting Production \(O,o)/')
    app.config.from_object('config')
    app.run(debug=False)
else:
    print('Starting Dev ¯\_(ツ)_/¯')
    app.config.from_object('config_dev')
    if __name__ == '__main__':
        app.run(debug=True)