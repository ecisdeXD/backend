from flask import render_template, Response

def renderTemplate(name,obj={}):
    return Response(render_template(name + '.html', data=obj), mimetype="text/html")