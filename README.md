# README #

Repositorio BACKEND para la práctica de ECSDI usando python 3.5.3

### Módulos necesarios ###

- Flask
- Flask-RESTful

Se pueden instalar manualmente con: ```pip3 install nombreDelModulo```

Tambien se pueden instalar todos a la vez con: ```pip3 install -r modules.txt```